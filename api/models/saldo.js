module.exports = (sequelize, DataTypes) => {
  const Saldo = sequelize.define('Saldo', {
    login: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    saldo: DataTypes.FLOAT
  }, {
    timestamps: false,
    createdAt: false,
    updatedAt: false,
    freezeTableName: true
  });
  Saldo.associate = function (models) {
    // associations can be defined here
  };
  return Saldo;
};