# Carteira Virtual - MaximaTech (Cayo Eduardo)
## Passos iniciais

- Clone o projeto do git no repositório público: https://gitlab.com/Cayo_Eduardo/carteira-virtual
- Crie o banco de dados PostgreSQL, utilizando o arquivo criar_banco.sql, na pasta bd.
- Crie as tabelas de dados PostgreSQL, utilizando o arquivo criar_tabelas.sql, na pasta bd.

## Conexão com o banco PostgreSQL

- Todas as credenciais do banco no arquivo /config/config.json
- Atualmente está sendo mantido pelo servidor gratuito do Heroku

## Intalando a API (back-end)

- cd api
- npm install
- npm run start (para executar o projeto)
- Obs.: verificar se a porte está sendo usada no arquivo /bin/www

## Intalando a Aplicação VueJS (front-end)

- cd api
- npm install
- npm run dev (para executar o projeto)

## Requisitos cumpridos da aplicação

- Acesse sua conta com um login e senha. (OK)
- Verifique um saldo na tela inicial. (OK)
- Tenha a opção de ver o histórico de transações de entrada e saida (debito e crédito) de dinheiro da sua conta. (OK)
- Consiga transferir créditos para a conta de um amigo, usando o nome de usuário/login dele. (OK)
- Deve ser possível criar um novo cadastro, em um formulário com login (obrigatório) senha (obrigatório) e nome (opcional). (OK)
- Quando uma nova conta for criada, deve ser creditado um valor de 100 reais. Deve haver esse registro na tabela SALDO (abaixo), sem uma movimentação correspondente. (OK)
- Toda transferência para um amigo deve deduzir do saldo da conta de origem e creditar o valor na conta de destino, além de registrar a transferência na tabela MOVIMENTACAO. (OK)

- A senha estar encriptada/com hash no banco usando SHA-256 ou algoritmo similar ou superior que impeça a leitura dela. (OK)

- Se além do repositório, o ambiente for entregue em algum lugar online já executando para avaliação. (OK)

## Ambiente Online

- Back-end (Servidor gratuito Heroku): https://carteira-digital-backend.herokuapp.com/
- Front-end (Servidor gratuito Heroku): https://carteira-digital-backend.herokuapp.com/

- Obs.: Em caso de falha no servidor Front-end, por favor, entrar em contato. Pois o Heroku não deixa o servidor no ar 24h por dia, ele inicia somente para uso.